package mathengine.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mathengine.business.Summation;

public class SummationServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String val1 = request.getParameter("x");
        String val2 = request.getParameter("y");
        String sum = Summation.sum(val1, val2);
        response.getWriter().write(sum);
    }
}
