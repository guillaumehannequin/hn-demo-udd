package mathengine.business;

public class Summation {

    private Summation() {
    }

    public static String sum(String val1, String val2) {
        int n1 = Integer.parseInt(val1);
        int n2 = Integer.parseInt(val2);
        int sum = n1 + n2;
        return String.valueOf(sum);
    }
}
