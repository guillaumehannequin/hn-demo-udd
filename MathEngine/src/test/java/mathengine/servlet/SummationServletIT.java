package mathengine.servlet;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

import org.junit.Test;

public class SummationServletIT {

    public static String readLine(String http) throws IOException {
        URL url = new URL(http);
        Scanner stream = new Scanner(url.openStream());
        String line = stream.nextLine();
        stream.close();
        return line;
    }

    @Test
    public void testSummation() throws IOException {
        assertEquals("5", readLine("http://localhost:8080/MathEngine/sum?x=2&y=3"));
    }
}
