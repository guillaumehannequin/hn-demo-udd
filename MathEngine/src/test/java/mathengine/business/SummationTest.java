package mathengine.business;

import static mathengine.business.Summation.sum;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SummationTest {

    @Test
    public void testSum() {
        assertEquals("5", sum("2", "3"));
    }
}
