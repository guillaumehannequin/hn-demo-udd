package plugins.canary;

import java.io.File;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "deploy")
@Execute(phase=LifecyclePhase.PACKAGE)
public class DeployMojo extends AbstractMojo {

   @Parameter(property = "project.build.directory")
   private String directory; // something like "target"

   @Parameter(property = "project.build.finalName")
   private String finalName; // name (without extension)

   @Parameter(property = "project.packaging")
   private String packaging; // jar, war, ear...

   @Parameter(property = "project.version")
   private String version;

   @Parameter(property = "oddLocation")
   private String oddLocation;

   @Parameter(property = "evenLocation")
   private String evenLocation;

   public void execute() throws MojoExecutionException {

       int ver;
       try {
           ver = Integer.parseInt(version);
       } catch (NumberFormatException e) {
           throw new MojoExecutionException("Version not valid: " + version, e);
       }

       String file = finalName + "." + packaging;
       File src = new File(directory, file);
       if (!src.exists())
           throw new MojoExecutionException("Source not found: " + src);
       getLog().info("File source: " + src);

       File dest;
       if (ver % 2 == 0) {
           getLog().info("Version " + version + " is even.");
           if (evenLocation == null || evenLocation.trim().isEmpty())
               throw new MojoExecutionException("Parameter evenLocation is required but missing.");
           dest = new File(evenLocation, file);
       } else {
           getLog().info("Version " + version + " is odd.");
           if (oddLocation == null || oddLocation.trim().isEmpty())
               throw new MojoExecutionException("Parameter oddLocation is required but missing.");
           dest = new File(oddLocation, file);
       }

       getLog().info("File destination: " + dest);

       if (dest.exists()) {
           getLog().info("Destination already exist and need to be deleted.");
           if (!dest.delete())
               throw new MojoExecutionException("Can't delete already existing destination: " + dest);
       }

       if (!src.renameTo(dest))
           throw new MojoExecutionException("Can't move " + src + " to " + dest);

       getLog().info("Move success");
   }
}
